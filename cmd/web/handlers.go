package main

import (
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"se07.com/pkg/models"
	"strconv"
)

func files() []string {
	files := []string{
		"./ui/html/base.html",
		"./ui/html/base.layout.tmpl.html",
		"./ui/html/footer.html",
		"./ui/html/header.html",
		"./ui/html/home.page.tmpl",
	}
	return files
}

func (app *application) home(resp http.ResponseWriter, req *http.Request) {
	if req.URL.Path != "/" { //for 404
		app.notFound(resp)
		return
	}
	files := files()

	ts, err := template.ParseFiles(files...)
	if err != nil {
		app.serverError(resp, err)
		return
	}
	err = ts.Execute(resp, nil)
	if err != nil {
		app.serverError(resp, err)
	}

	/*
		s, err := app.snippets.Latest()
		if err != nil {
			app.serverError(resp, err)
			return
		}

		for _, snippet := range s {
			fmt.Fprintf(resp, "%v\n", snippet)
		}*/
}

func (app *application) createSnippet(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost { //if get
		w.Header().Set("Allow", http.MethodPost)
		app.clientError(w, http.StatusMethodNotAllowed)
		return
	}
	title := "O snail"
	content := "O snail\nClimb Mount Fuji,\nBut slowly, slowly!\n\n– Kobayashi Issa"
	expires := "7"

	id, err := app.snippets.Insert(title, content, expires)
	if err != nil {
		app.serverError(w, err)
		return
	}
	fmt.Fprint(w, id)
	/*
		w.Write([]byte("Create anew snippet"))
			http.Redirect(w, r, fmt.Sprintf("/snippet?id=%d", id), http.StatusSeeOther)
	*/
}

func (app *application) showSnippet(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(r.URL.Query().Get("id")) //get value from url
	if err != nil || id < 1 {                        //foe error
		app.notFound(w)
		return
	}

	s, err := app.snippets.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
		return
	}

	data := &templateData{Snippet: s}
	files := files()

	ts, err := template.ParseFiles(files...)
	if err != nil {
		app.serverError(w, err)
		return
	}
	err = ts.Execute(w, data)
	if err != nil {
		app.serverError(w, err)
	}

	//fmt.Fprintf(w, "%v", s)
}
